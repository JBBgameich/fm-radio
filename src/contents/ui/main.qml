import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.2 as Controls
import QtMultimedia 5.8
import QtQuick.Layouts 1.1

Kirigami.ApplicationWindow {
    id: root

    title: "FM-Radio"

    header: Kirigami.ApplicationHeader {}

    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            enabled: true
            title: {
                if (!radio.antennaConnected)
                    return "Please connect your headphones!"
                else
                    return "FM-Radio"
            }

            // Radio component
            Radio {
                id: radio
                band: Radio.FM
            }

            ColumnLayout {
                anchors.fill: parent

                Kirigami.Heading {
                    Layout.alignment: Qt.AlignHCenter
                    text: radio.frequency + " Hz"
                }

                Controls.Slider {
                    id: slider
                    Layout.fillWidth: true

                    from: radio.minimumFrequency
                    to: radio.maximumFrequency
                    orientation: Qt.Horizontal

                    value: radio.frequency

                    Controls.ToolTip {
                        parent: slider.handle
                        visible: slider.pressed
                        text: slider.position.toFixed(1)
                    }
                }
            }

            // Action buttons
            actions {
                main: Kirigami.Action {
                    text: "Start / Stop"
                    iconName: {
                        if (radio.state == Radio.StoppedState)
                            return "media-playback-start"
                        else
                            return "media-playback-pause"
                    }
                    onTriggered: {
                        if (radio.state == Radio.StoppedState)
                            return radio.start()
                        else
                            return radio.stop()
                    }
                }
                left: Kirigami.Action {
                    text: "Tune to previous station"
                    iconName: "go-previous"
                    onTriggered: radio.scanDown()
                }
                right: Kirigami.Action {
                    text: "Tune to next station"
                    iconName: "go-next"
                    onTriggered: radio.scanUp()
                }
            }
        }
    }
}
